drop database if exists clients;
CREATE DATABASE clients;

USE clients;

create table users
(
    id int
    auto_increment primary key,
  name  text null,
  email text null,
  phone text null
);

    create table devices
    (
        id int
        auto_increment primary key,
        user_id     int  not null,
        mac_address text null,
        ipv4        text null,
        ipv6        text null
);
