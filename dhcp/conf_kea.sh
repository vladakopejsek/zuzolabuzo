#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 127
fi

if [ -f /etc/kea/kea.conf ]; then
	echo "Backing up original kea.conf -> kea.conf.default"
	mv /etc/kea/kea.conf /etc/kea/kea.conf.default
fi

mkdir -p /var/kea
touch /var/kea/kea.log

cp /vagrant/dhcp/kea.conf /etc/kea/kea.conf

mysql -h 192.168.50.100 -n dhcp --user dhcp --password='heslo' < /usr/share/kea/scripts/mysql/dhcpdb_create.mysql

rm /var/lib/kea/kea*.pid

exit 0
