# Virtuální síť

## Zadání

Vytvořte virtuální síť, která se skládá z DHCP serveru, DNS serveru a databáze.
K vytvoření virtuální sítě použijte nástroj Vagrant v kombinaci s nástrojem Ansible, který slouží k automatizaci zpracování jednotlivých skriptů.
V týmu si rozdělte role - co kdo bude dělat. K projektu pak sestavte dokumentaci.

## Tým

* Štěpán Galle (Vedoucí, dokumentace, nějaké pokusy pomoct při dělání DNS, DHCP, MySQL)
* Vladimír Kopejska (DNS, DHCP, dokumentace)
* Martin Košek (MySQL, dokumentace)

## Použité technologie a programy
Vycházeli jsme ze zadání, takže jsme se rozhodli použít právě všechny v něm uvedené technologie.

### Vagrant
Stará se o správu jednotlivých virtuálních strojů, ale ne-emuluje je. Teoreticky se dá říct, že struktura celé sítě je vytvořena právě v něm.
Přes CLI se dá tedy velice jednoduše a efektivně spravovat celá virtuální síť.
Funguje na principu "boxů", které je uživatel shopný jednoduše stáhnout a poté aplikovat a instalovat na jednotlivá virtuální zařízení, která si definoval v konfiguračním souboru Vagrantu.
Těmto zařízením může mimo jiné přiřadit například statickou IP adresu, nebo "provision" soubor, který může býť buď přímo shell skript, nebo v našem případě Ansible "playbook".
Provision soubor se zpracuje při spouštění jednotlivých strojů, nebo také manuálním příkazem. Definicí úloh v tomto souborů je například možné jednoduše nainstalovat na jednotlivé stroje potřebné balíky, nebo konfiguraci služeb. Není pak tedy nutné pracovat na zařízeních jednotlivě a provádět všechny příkazy manuálně, ale je možné si připravit tuto automatizaci.

### Ansible
Nástroj sloužící k automatizaci jednotlivých úloh, které slouží k instalaci balíků a konfiguraci jednotlivých strojů. Je skvělou náhradou pro použití přímých shell skriptů (také kvůli zjednodušenému zápisu).

### CentOS
Linuxová distribuce, která je vhodná pro síťové účely. Na jednotlivých zařízeních jsme ho instalovali za použití Vagrant boxu (centos/7).

### VirtualBox
Program, který se stará o emulaci a běh jednotlivých virtuálních strojů.

## Výsledek práce
Kvůli ne příliš velké znalosti jednotlivých funkčností a vnitřní struktuře jednotlivých aspektů sítě jsme bohužel nebyli schopni správně nastavit DNS server a dalších věcí, o kterých ani nevíme.

### DHCP
Vagrantfile konfigurace DHCP serveru.
```bash
config.vm.define "dhcp", primary: true do |server|
    dhcp.vm.box = "centos/7"
    dhcp.vm.network "private_network", ip: "192.168.50.101"
    
    dhcp.vm.provision "ansible" do |ansible|
        ansible.verbose = "vvv"
        ansible.playbook = "./provisions/dhcp.yml"
    end
end
```

Ansible playbook obsahující úlohy pro nastavení DHCP serveru.
```yaml
---
- hosts: dhcp
  sudo: false
  vars:
  tasks:
    - name: Install KEA server
      become: yes
      become_user: root
      become_method: sudo
      yum:
          name: kea, mariadb, mariadb-server
          state: present

    - name: Start mysql
      become: yes
      become_user: root
      become_method: sudo
      service:
          name: mariadb
          state: started
          enabled: true

    - name: Configure KEA
      become: yes
      become_user: root
      become_method: sudo
      script: /vagrant/conf/conf_kea.sh
```


### DNS
### Databáze
Vagrantfile konfigurace stroje s databází.

```bash
config.vm.define "server", primary: true do |server|
    server.vm.box = "centos/7"
    server.vm.network "private_network", ip: "192.168.50.100"
    
    server.vm.provision "ansible" do |ansible|
        ansible.verbose = "vvv"
        ansible.playbook = "./provisions/server.yml"
    end
end
```

## Zdroje
* Dokumentace Vagrantu
    * https://www.vagrantup.com/docs/index.html
* Dokumentace Ansible
    * https://docs.ansible.com/
* Dokumentace VirtualBox
    * https://www.virtualbox.org/wiki/Documentation